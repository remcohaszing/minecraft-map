#!/usr/bin/env python3
import io
import json
import os
import shutil
import tarfile

import jwt
import requests


def get_access_token(email, password):
    print("Receiving access token…")
    response = requests.post(
        "https://authserver.mojang.com/authenticate",
        json.dumps(
            {
                "agent": {"name": "Minecraft", "version": 1},
                "clientToken": "any string",
                "password": password,
                "username": email,
            }
        ),
        headers={"User-Agent": "Minecraft/1"},
    )
    if response.status_code != 200:
        raise Exception(response.text)
    print("✔️ Received access token succesfully")
    return response.json()["accessToken"]


def get_uuid(access_token):
    print("Extracting uuid from access token…")
    uuid = jwt.decode(access_token, verify=False)["spr"]
    print("✔️ Extracted uuid succesfully")
    return uuid


def get_username(uuid):
    print("Getting player name…")
    response = requests.get(f"https://api.mojang.com/user/profiles/{uuid}/names").json()
    name = response[-1]["name"]
    print("✔️ Received username succesfully")
    return name


def get_world_id(name, cookies):
    """
    Get the world id by its name.

    """
    print("Receiving world id")
    r = requests.get("https://pc.realms.minecraft.net/worlds", cookies=cookies)
    worlds = r.json()["servers"]
    for world in worlds:
        if world["name"] == name:
            print("✔️ Received world id succesfully")
            return world["id"]
    raise Exception("No worlds found")


def get_download_link(world_id, slot, cookies):
    print("Receiving world download link…")
    r = requests.get(
        f"https://pc.realms.minecraft.net/worlds/{world_id}/slot/{slot}/download",
        cookies=cookies,
    )
    print("✔️ Received world download link succesfully")
    return r.json()["downloadLink"]


def download_world(download_link):
    if os.path.exists("world"):
        print("Deleting old world directory")
        shutil.rmtree("world")
        print("✔️ Deleted old world directory succesfully")
    print("Downloading and extracting world…")
    r = requests.get(download_link, stream=True)
    fileobj = io.BytesIO(r.content)
    tar = tarfile.open(fileobj=fileobj)
    tar.extractall()
    print("✔️ Downloaded and extraced world succesfully")


def main(email, password, world, slot=1):
    access_token = get_access_token(email, password)
    uuid = get_uuid(access_token)
    username = get_username(uuid)
    cookies = {
        "sid": f"token:{access_token}:{uuid}",
        "user": username,
        "version": "1.11.2",
    }
    world_id = get_world_id(world, cookies)
    download_link = get_download_link(world_id, slot, cookies)
    print(download_link)
    download_world(download_link)


if __name__ == "__main__":
    assert "MC_EMAIL" in os.environ, "Specify environment variable MC_EMAIL"
    assert "MC_PASSWORD" in os.environ, "Specify environment variable MC_PASSWORD"
    assert "MC_WORLD" in os.environ, "Specify environment variable MC_WORLD"
    main(
        os.environ["MC_EMAIL"],
        os.environ["MC_PASSWORD"],
        os.environ["MC_WORLD"],
        os.environ.get("MC_WORLD_SLOT", 1),
    )
