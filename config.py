worlds["world"] = "world"
texturepath = "/"
outputdir = "public"

renders["day"] = {
    "world": "world",
    "rendermode": "smooth_lighting",
    "title": "Daytime",
}

renders["night"] = {
    "world": "world",
    "rendermode": "night",
    "title": "Night",
}
