FROM ubuntu:19.10 AS build
RUN apt-get update \
 && apt-get install --yes --no-install-recommends \
        ca-certificates \
        gcc \
        git \
        python3-dev \
        python3-distutils \
        python3-numpy \
        python3-pillow \
 && git clone --branch v0.15.0 --depth 1 https://github.com/overviewer/Minecraft-Overviewer.git /build \
 && python3 /build/setup.py install \
 && chown -R root:root /usr/local/lib/python3.7

FROM ubuntu:19.10
COPY --from=build /usr/local/lib/python3.7 /usr/local/lib/python3.7
COPY --from=build /usr/local/bin/overviewer.py /usr/local/bin/overviewer.py
COPY download-world.py /usr/local/bin/download-world
COPY assets /assets
COPY config.py /config.py
RUN apt-get update \
 && apt-get install --yes --no-install-recommends \
        python3-jwt \
        python3-numpy \
        python3-pillow \
        python3-requests \
 && apt-get clean \
 && useradd --shell /bin/bash --create-home minecraft
USER minecraft
